% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2011 Robert C. Green II
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS
function [bestFitnessOverall,bestProbeNumberOverall, bestTimeStepOverall, bestNumProbesPerDimension, bestGamma, Rbest, Mbest ,Abest, bestNumProbes, bestLastStep] = parameterFreeCFO(Nt, Nd, oMin, oMax, functionName)

    
    Nt = 1000;
    bestFitnessOverall = -Inf;

    Abest = [];    Rbest = [];    Mbest = [];
    
    deltaFrep   = 0.1;
    Frep        = 0.5;
    Alpha       = 1.0;
    Beta        = 2.0;
    numGammas   = 21;
    maxProbesPerDimension = getMaxProbes(Nd);
    
    xMin = zeros(1, Nd, 'double');
    xMax = zeros(1, Nd, 'double');
    
    [x y] = size(oMin);
    if x~= 1 && y ~= 1
        for i=1:Nd
            xMin(i) = oMin(i); xMax(i) = oMax(i);
        end
    else
        for i=1:Nd
            xMin(i) = oMin; xMax(i) = oMax;
        end
    end
        
    for numProbesPerAxis = 4:maxProbesPerDimension
        Np = numProbesPerAxis * Nd;
        
        for GammaNumber = 1:numGammas
            Gamma = (double(GammaNumber)-1)/(double(numGammas)-1);
            
            bestFitnessThisRun      = Nt;
            bestProbeNumberThisRun 	= 0; 
            lastStep = Nt;
            
            [bestFitnessThisRun, bestProbeNumberThisRun, bestTimeStepThisRun, lastStep, R, M, A] = CFO(Np, Nd, Nt, xMin, xMax, functionName, Alpha, Beta, Gamma, Frep, deltaFrep);
           
            if bestFitnessThisRun >= bestFitnessOverall
                bestFitnessOverall          = bestFitnessThisRun;
                bestProbeNumberOverall      = bestProbeNumberThisRun;
                bestTimeStepOverall         = bestTimeStepThisRun;
                bestNumProbesPerDimension   = numProbesPerAxis;
                bestNumProbes               = Np;
                bestGamma                   = Gamma;
                bestLastStep                = lastStep;
                Rbest = R;   Mbest = M;  Abest = A;
            end
        end % End Gamma
    end % End Probes
end

