% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function Z = F19(R, Nd, p, j)
%F19 Summary of this function goes here
%   Detailed explanation goes here
    
    Aji = zeros(4,3,'double');
    Cj  = zeros(1,4,'double');
    Pji = zeros(4,3,'double');
    
    Aji(1,1) = 3.0; Aji(1,2) = 10; Aji(1,3) = 30; Cj(1) = 1.0;
    Aji(2,1) = 0.1; Aji(2,2) = 10; Aji(2,3) = 35; Cj(2) = 1.2;
    Aji(3,1) = 3.0; Aji(3,2) = 10; Aji(3,3) = 30; Cj(3) = 3.0;
    Aji(4,1) = 0.1; Aji(4,2) = 10; Aji(4,3) = 35; Cj(4) = 3.2;

    Pji(1,1) = 0.36890; Pji(1,2) = 0.1170; Pji(1,3) = 0.2673;
    Pji(2,1) = 0.46990; Pji(2,2) = 0.4387; Pji(2,3) = 0.7470;
    Pji(3,1) = 0.10910; Pji(3,2) = 0.8732; Pji(3,3) = 0.5547;
    Pji(4,1) = 0.03815; Pji(4,2) = 0.5743; Pji(4,3) = 0.8828;

    Z = 0;

    for jj = 1:4
        Sum = 0;
        for i = 1:3
            Xi = R(p,i,j);
            Sum = Sum + Aji(jj,i)*(Xi-Pji(jj,i))^2;

        end
        Z = Z + Cj(jj)*exp(-Sum);
    end
end

