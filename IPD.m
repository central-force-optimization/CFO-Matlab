% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function R = IPD( Np, Nd, R, xMin, xMax, Gamma, place)
%IPD Summary of this function goes here
%   Detailed explanation goes here
   
    if nargin < 7
        place = 'UNIFORM ON-AXIS';
    end
    
    switch place
        case 'UNIFORM ON-AXIS'
            numProbesPerDimension = Np;

            if Nd > 1
                numProbesPerDimension = Np/Nd;
            end

            for i=1:Nd
                for p=1:Np
                    R(p,i,1) = xMin(i) + Gamma * (xMax(i) - xMin(i));
                end
            end

            for i=1:Nd
                DeltaXi = (xMax(i)-xMin(i))/(numProbesPerDimension-1);
                for k=1:numProbesPerDimension
                    p = k + numProbesPerDimension * (i-1);
                    R(p,i,1) = xMin(i) + (k-1)*DeltaXi;
                end
            end
        case 'UNIFORM ON-DIAGONAL'
            for p=1:Np
                for i=1:Nd
                    DeltaXi = (xMax(i)-xMin(i))/(Np-1);
                    R(p,i,1) = xMin(i) + (p-1)*DeltaXi;
                end
            end
        case 'RANDOM'
            for p=1:Np
                for i=1:Nd
                    R(p,i,1) = xMin(i) + rand*(xMax(i)-xMin(i));
                end
            end
    end
end
