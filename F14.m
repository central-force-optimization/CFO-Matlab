% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to; CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to; Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function Z = F14(R, Nd, p, j)

    Aij(1,1)    =-32; Aij(1,2)  =-16;   Aij(1,3)=0  ; Aij(1,4)=16  ; Aij(1,5)=32;
    Aij(1,6)    =-32; Aij(1,7)  =-16;   Aij(1,8)=0  ; Aij(1,9)=16  ; Aij(1,10)=32;
    Aij(1,11)   =-32; Aij(1,12) =-16;   Aij(1,13)=0; Aij(1,14)=16; Aij(1,15)=32;
    Aij(1,16)   =-32; Aij(1,17) =-16;   Aij(1,18)=0; Aij(1,19)=16; Aij(1,20)=32;
    Aij(1,21)   =-32; Aij(1,22) =-16;   Aij(1,23)=0; Aij(1,24)=16; Aij(1,25)=32;

    Aij(2,1)    =-32; Aij(2,2)  =-32;   Aij(2,3)=-32; Aij(2,4)=-32; Aij(2,5)=-32;
    Aij(2,6)    =-16; Aij(2,7)  =-16;   Aij(2,8)=-16; Aij(2,9)=-16; Aij(2,10)=-16;
    Aij(2,11)   = 0;  Aij(2,12) = 0 ;   Aij(2,13)=0  ; Aij(2,14)=0  ; Aij(2,15)=0;
    Aij(2,16)   = 16; Aij(2,17) =16;    Aij(2,18)=16; Aij(2,19)=16; Aij(2,20)=16;
    Aij(2,21)   = 32; Aij(2,22) =32;    Aij(2,23)=32; Aij(2,24)=32; Aij(2,25)=32;
    Sum1 = 0;

    for jj=1:25
        Sum2 = 0;

        for i=1:2

            Xi = R(p,i,j);

            Sum2 = Sum2 + (Xi-Aij(i,jj))^6;

        end

        Sum1 = Sum1 + 1/(jj+Sum2);

    end

    Z = 1/(0.002+Sum1);

    Z = -Z;
end