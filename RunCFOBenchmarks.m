% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS


function RunCFOBenchmarks
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    functionName = 'SGO';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F1';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F2';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F3';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F4';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F5';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F6';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F7';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F8';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F9';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F10';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F11';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F12';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F13';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F14';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F15';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F16';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F17';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F18';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F19';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F20';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'F21';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

    functionName = 'SCHWEFEL_226';
    [Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, xMin, xMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters(functionName);
    [fitness,probe, timeStep, numProbesPerDimension, Gamma, R, M ,A, Np, lastStep ] = parameterFreeCFO(Nt, Nd, xMin, xMax, functionName);
    DisplayResults( functionName, fitness, probe, timeStep, Gamma, numProbesPerDimension, Np, lastStep )

end

