% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2011 Robert C. Green II
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS
function [bestFitness, bestProbeNumber, bestTimeStep, lastStep, R, M ,A] =  CFO2()
    
    % Initialize
    functionName = 'F1'; % Name of function and file. function F1(R, Nd, p, j) is in F1.m
    Np      = 60;   Nd          = 30;   Nt          = 1000;
    Alpha   = 2;    Beta        = 2;    Gamma       = 0.45;
    Frep    = 0.5;  deltaFrep   = 0.1; 
    
    for i=1:Nd 
        xMin(i) = -100; 
        xMax(i) = 100; 
    end
       
    objective       = str2func(functionName);
    bestFitness     = -Inf;
    bestProbeNumber = 0; 
    bestTimeStep    = 0;
    lastStep        = 0;
                
    bestFitnessHistory = [];
    bestPosition = [];
    A = zeros(Np, Nd, 'double');
    R = zeros(Np, Nd, 'double');
    M = zeros(Np, 'double');
                
    % Initial Probe Distribution
    % UNIFORM ON-AXIS
    numProbesPerDimension = Np;
    if Nd > 1
        numProbesPerDimension = Np/Nd;
    end
    for p=1:Np
        R(p,:) = xMin + Gamma.*(xMax - xMin);
    end
    
    for i=1:Nd
        DeltaXi = (xMax(i)-xMin(i))/(numProbesPerDimension-1);
        for k=1:numProbesPerDimension
            p = k + numProbesPerDimension * (i-1);
            R(p,i) = xMin(i) + (k-1)*DeltaXi;
        end
    end
%    UNIFORM ON-DIAGONAL'
%    for p=1:Np
%        for i=1:Nd
%            DeltaXi = (xMax(i)-xMin(i))/(Np-1);
%            R(p,i,1) = xMin(i) + (p-1)*DeltaXi;
%        end
%    end
%    RANDOM
%    for p=1:Np
%        for i=1:Nd
%            R(p,i,1) = xMin(i) + rand*(xMax(i)-xMin(i));
%        end
%    end
            
    % Set Initial Acceleration to 0
    for p=1:Np
        for i=1:Nd
            A(p,i,1) = 0.0;
        end
    end
      
    % Compute Objective Function
    for p=1:Np
        M(p) = objective(R, Nd, p);
        if M(p) >= bestFitness
            bestPosition    = R(p,:);
            bestFitness     = M(p);
            bestTimeStep    = 1;
            bestProbeNumber = p;
        end
    end
            
    lastStep = Nt;
    for j=2:Nt
    
        R(:,:) = R(:,:) +  A(:,:);

        for p=1:Np
            for i=1:Nd
                if R(p,i) < xMin(i)
                    R(p,i) = max(xMin(i) + Frep*(R(p,i)-xMin(i)), xMin(i));
                end
                if R(p, i) > xMax(i)
                    R(p,i) = min(xMax(i) - Frep*(xMax(i) -R(p,i)), xMax(i));
                end
            end
        end

        for p=1:Np
            M(p) = objective(R, Nd, p);
            if M(p) >= bestFitness
                bestPosition    = R(p,:);
                bestFitness     = M(p);
                bestTimeStep    = j;
                bestProbeNumber = p;
            end
        end
        bestFitnessHistory(j-1) = bestFitness;

        for p=1:Np
            for i=1:Nd
                A(p,i) = 0.0;
                for k=1:Np
                    if k ~= p
                        SumSQ = 0.0;
                        for L=1:Nd
                            SumSQ = SumSQ + (R(k,L)-R(p,L))^2;
                        end
                        if SumSQ ~= 0
                           Denom     = sqrt(SumSQ);
                           Numerator = UnitStep(M(k)-M(p))*(M(k)-M(p));
                           A(p,i)  = A(p,i) + (R(k,i)-R(p,i)) * (Numerator^Alpha)/(Denom^Beta);
                        end
                    end
                end
            end
        end

        Frep = Frep + deltaFrep;
        if Frep > 1
            Frep = 0.05;
        end

        if mod(j, 10) == 0 &&  j >= 10
            xMin = xMin + (bestPosition-xMin)./2 ;
            xMax = xMax - (xMax - bestPosition)./2;
            for p=1:Np
                for i=1:Nd
                    if R(p,i) < xMin(i)
                        R(p,i) = max(xMin(i) + Frep*(R(p,i)-xMin(i)), xMin(i));
                    end
                    if R(p, i) > xMax(i)
                        R(p,i) = min(xMax(i) - Frep*(xMax(i) -R(p,i)), xMax(i));
                    end
                end
            end
        end

%        if hasFitnessSaturated(25, j, Np, Nd, M, R) == true
%            lastStep = j;
%            break;
%        end
        str = sprintf('Iteration: %d Best Value: %f', j, bestFitness);
        disp(str)
sleep(2)
    end % End Time Step Loop        
    plot(bestFitnessHistory);
    str = sprintf('Last Step: %d Best Value: %f', lastStep, bestFitness);
    disp(str)
    disp(bestPosition);
end

function out = hasFitnessSaturated(nSteps, j, Np, Nd, M, R)
    FitnessSatTol = 0.000001;
    out = false;
    
    if j < nSteps + 10
        return;
    end
    SumOfBestFitnesses = 0;    
    for k=j-nSteps+1:j
       bestFitness = -Inf;
       for p=1:Np
          if M(p,k) >= bestFitness
              bestFitness = M(p,k);
          end
       end
       if k == j
          bestFitnessStepJ = bestFitness; 
       end
       SumOfBestFitnesses = SumOfBestFitnesses + bestFitness;
    end
    
    if  abs(SumOfBestFitnesses/nSteps-bestFitnessStepJ) <= FitnessSatTol
        out = true;
    end

end

