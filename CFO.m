% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2011 Robert C. Green II
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS
function [bestFitness, bestProbeNumber, bestTimeStep, lastStep, R, M ,A] =  CFO(Np, Nd, Nt, xMin, xMax, functionName, Alpha, Beta, Gamma, Frep, deltaFrep)
    
    % Initialize
    objective       = str2func(functionName);
    bestFitness     = -Inf;
    bestProbeNumber = 0; 
    bestTimeStep    = 0;
    lastStep        = 0;
                
    bestFitnessHistory = [];
    A = zeros(Np, Nd, Nt, 'double');
    R = zeros(Np, Nd, Nt, 'double');
    M = zeros(Np, Nt, 'double');
                
    % Initial Probe Distribution
    % UNIFORM ON-AXIS
    numProbesPerDimension = Np;
    if Nd > 1
        numProbesPerDimension = Np/Nd;
    end
    for i=1:Nd
        for p=1:Np
            R(p,i,1) = xMin(i) + Gamma * (xMax(i) - xMin(i));
        end
    end
    for i=1:Nd
        DeltaXi = (xMax(i)-xMin(i))/(numProbesPerDimension-1);
        for k=1:numProbesPerDimension
            p = k + numProbesPerDimension * (i-1);
            R(p,i,1) = xMin(i) + (k-1)*DeltaXi;
        end
    end
    %UNIFORM ON-DIAGONAL'
    %for p=1:Np
    %    for i=1:Nd
    %        DeltaXi = (xMax(i)-xMin(i))/(Np-1);
    %        R(p,i,1) = xMin(i) + (p-1)*DeltaXi;
    %    end
    %end
    %RANDOM
    %for p=1:Np
    %    for i=1:Nd
    %        R(p,i,1) = xMin(i) + rand*(xMax(i)-xMin(i));
    %    end
    %end
            
    % Set Initial Acceleration to 0
    for p=1:Np
        for i=1:Nd
            A(p,i,1) = 0.0;
        end
    end
      
    % Compute Objective Function
    for p=1:Np
        M(p,1) = objective(R, Nd, p, 1);
        if M(p,1) >= bestFitness
            bestFitness     = M(p,1);
            bestTimeStep    = 1;
            bestProbeNumber = p;
        end
    end
     bestFitnessHistory(1) = bestFitness;
            
    lastStep = Nt;
    for j=2:Nt
        for p=1:Np
            for i=1:Nd
                R(p,i,j) = R(p,i,j-1) +  A(p,i,j-1);
            end
        end

        for p=1:Np
            for i=1:Nd
                if R(p,i,j) < xMin(i)
                    R(p,i,j) = max(xMin(i) + Frep*(R(p,i,j-1)-xMin(i)), xMin(i));
                end
                if R(p, i, j) > xMax(i)
                    R(p,i,j) = min(xMax(i) - Frep*(xMax(i) -R(p,i,j-1)), xMax(i));
                end
            end
        end

        for p=1:Np
            M(p,j) = objective(R, Nd, p, j);
            if M(p,j) >= bestFitness
                bestFitness     = M(p,j);
                bestTimeStep    = j;
                bestProbeNumber = p;
            end
        end
        bestFitnessHistory(j) = bestFitness;

        for p=1:Np
            for i=1:Nd
                A(p,i,j) = 0.0;
                for k=1:Np
                    if k ~= p
                        SumSQ = 0.0;
                        for L=1:Nd
                            SumSQ = SumSQ + (R(k,L,j)-R(p,L,j))^2;
                        end
                        if SumSQ ~= 0
                           Denom = sqrt(SumSQ);
                           Numerator = UnitStep(M(k,j)-M(p,j))*(M(k,j)-M(p,j));
                           A(p,i,j) = A(p,i,j) + (R(k,i,j)-R(p,i,j)) * (Numerator^Alpha)/(Denom^Beta);
                        end
                    end
                end
            end
        end

        Frep = Frep + deltaFrep;
        if Frep > 1
            Frep = 0.05;
        end

        if mod(j, 10) == 0 &&  j >= 10
            for i=1:Nd
               xMin(i) = xMin(i) + (R(bestProbeNumber,i,bestTimeStep)-xMin(i))/2 ;
               xMax(i) = xMax(i) - (xMax(i) - R(bestProbeNumber,i,bestTimeStep))/2 ;
            end
            for p=1:Np
                for i=1:Nd
                    if R(p,i,j) < xMin(i)
                        R(p,i,j) = max(xMin(i) + Frep*(R(p,i,j-1)-xMin(i)), xMin(i));
                    end
                    if R(p, i, j) > xMax(i)
                        R(p,i,j) = min(xMax(i) - Frep*(xMax(i) -R(p,i,j-1)), xMax(i));
                    end
                end
            end
        end

        if hasFitnessSaturated(25, j, Np, Nd, M, R) == true
            lastStep = j;
            break;
        end
        
        plot(bestFitnessHistory);
        str = sprintf('Iteration: %d Best Value:%f', j, bestFitness);
        disp(str)
    end % End Time Step Loop        
end

function out = hasFitnessSaturated(nSteps, j, Np, Nd, M, R)
    FitnessSatTol = 0.000001;
    out = false;
    
    if j < nSteps + 10
        return;
    end
    SumOfBestFitnesses = 0;    
    for k=j-nSteps+1:j
       bestFitness = -Inf;
       for p=1:Np
          if M(p,k) >= bestFitness
              bestFitness = M(p,k);
          end
       end
       if k == j
          bestFitnessStepJ = bestFitness; 
       end
       SumOfBestFitnesses = SumOfBestFitnesses + bestFitness;
    end
    
    if  abs(SumOfBestFitnesses/nSteps-bestFitnessStepJ) <= FitnessSatTol
        out = true;
    end

end

