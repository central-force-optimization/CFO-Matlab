% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function R = RetrieveErrantProbes(Np, Nd, j, R, Frep, xMin, xMax)
%RETRIEVEERRANTPROBES Summary of this function goes here
%   Detailed explanation goes here

    for p=1:Np
        for i=1:Nd
            if R(p,i,j) < xMin(i)
                R(p,i,j) = max(xMin(i) + Frep*(R(p,i,j-1)-xMin(i)), xMin(i));
            end
            if R(p, i, j) > xMax(i)
                R(p,i,j) = min(xMax(i) - Frep*(xMax(i) -R(p,i,j-1)), xMax(i));
            end
        end
    end
end

