% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function [ Nd, Np, Nt, G, Alpha, Beta, DeltaT, Frep, XiMin, XiMax, DiagLength, PlaceInitialProbes, InitialAcceleration, RepositionFactor] = GetTestFunctionParameters( functionName )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    Np       = 0;
    Nd       = 0;
    Nt       = 500;
    G        = 2.0;
    Alpha    = 2.0;
    Beta     = 2.0;
    DeltaT   = 1.0;
    Frep     = 0.5;

    PlaceInitialProbes  = 'UNIFORM ON-AXIS';
    InitialAcceleration = 'ZERO';
    RepositionFactor    ='VARIABLE';
    
    switch functionName

        case 'ParrottF4'

            Nd = 1; Np = 3;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); XiMin(1) = 0; XiMax(1) = 1;

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'SGO'

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -50; XiMax(i) = 50; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'GP'

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'STEP'

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'SCHWEFEL_226'

            Nd = 30; Np = 120;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -500; XiMax(i) = 500; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'COLVILLE'

            Nd = 4; Np = 16;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -10; XiMax(i) = 10; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'GRIEWANK'

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -600; XiMax(i) = 600; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'HIMMELBLAU'

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -6; XiMax(i) = 6; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'ROSENBROCK' %(n-D)

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -2; XiMax(i) = 2; end;%XiMin(i) = -6; XiMax(i) = 6; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'SPHERE' %(n-D)

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'HIMMELBLAUNLO' %(5-D)

            Nd = 5; Np = 20;

            XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double');

            XiMin(1) = 78; XiMax(1) = 102;
            XiMin(2) = 33; XiMax(2) = 45;
            XiMin(3) = 27; XiMax(3) = 45;
            XiMin(4) = 27; XiMax(4) = 45;
            XiMin(5) = 27; XiMax(5) = 45;

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'TRIPOD' %(2-D)

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'ROSENBROCKF6' %(10-D)

            Nd = 10;
            Np = 40;

            XiOffset = zeros(Nd,'double');

            XiOffset(1)  =   5;
            XiOffset(2)  = -25;
            XiOffset(3)  =   5;
            XiOffset(4)  = -15;
            XiOffset(5)  =   5;
            XiOffset(6)  = -25;
            XiOffset(7)  =  25;
            XiOffset(8)  =  -5;
            XiOffset(9)  =   5;
            XiOffset(10) = -15;
            
             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

         case 'COMPRESSIONSPRING' %(3-D)

            Nd = 3; Np = 12;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double');

            XiMin(1) = 1    ; XiMax(1) =  70; %integer values only!!
            XiMin(2) = 0.6  ; XiMax(2) =   3;
            XiMin(3) = 0.207; XiMax(3) = 0.5;

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

         case 'GEARTRAIN' %(4-D)

            Nd = 4; Np = 16;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = 12; XiMax(i) = 60; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'F1' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F2' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -10; XiMax(i) = 10; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

      case 'F3' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

      case 'F4' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

      case 'F5' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -30; XiMax(i) = 30; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F6' %(n-D) STEP

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -100; XiMax(i) = 100; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F7' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -1.28; XiMax(i) = 1.28; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F8' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -500; XiMax(i) = 500; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F9' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -5.12; XiMax(i) = 5.12; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F10' %(n-D) Ackley%s Function

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -32; XiMax(i) = 32; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F11' %(n-D)

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -600; XiMax(i) = 600; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

        case 'F12' %(n-D) Penalized 1

            Nd = 30; Np = 60;
             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -50; XiMax(i) = 50; end

%            XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -5; XiMax(i) = 5; end %use this interval for second run to improve performance

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F13' %(n-D) Penalized 2

            Nd = 30; Np = 60;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -50; XiMax(i) = 50; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F14' %(2-D) Shekel%s Foxholes

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -65.536; XiMax(i) = 65.536; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F15' %(4-D) Kowalik%s Function

            Nd = 4;
            Np = 16;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -5; XiMax(i) = 5; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F16' %(2-D) Camel Back

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = -5; XiMax(i) = 5; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F17' %(2-D) Branin

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); XiMin(1) = -5; XiMax(1) = 10; XiMin(2) = 0; XiMax(2) = 15;

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F18' %(2-D) Goldstein-Price

            Nd = 2; Np = 8;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); XiMin(1) = -2; XiMax(1) = 2; XiMin(2) = -2; XiMax(2) = 2;

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F19' %(3-D) Hartman%s Family 1

            Nd = 3; Np = 12;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = 0; XiMax(i) = 1; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F20' %(6-D) Hartman%s Family 2

            Nd = 6; Np = 24;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = 0; XiMax(i) = 1; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F21' %(4-D) Shekel%s Family m=5

            Nd = 4; Np = 16;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = 0; XiMax(i) = 10; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F22' %(4-D) Shekel%s Family m=7

            Nd = 4; Np = 16;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = 0; XiMax(i) = 10; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

       case 'F23' %(4-D) Shekel%s Family m=10

            Nd = 4; Np = 16;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = 0; XiMax(i) = 10; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

         case 'PBM_1' %2-D

            Nd                    = 2;
            NumProbesPerDimension = 2; %4 %20
            Np                    = NumProbesPerDimension*Nd;

            Nt       = 100;
            G        = 2;
            Alpha    = 2;
            Beta     = 2;
            DeltaT   = 1;
            Frep     = 0.5;

            PlaceInitialProbes  = 'UNIFORM ON-AXIS';
            InitialAcceleration = 'ZERO';
            RepositionFactor    = 'VARIABLE'; %'FIXED'

            Np = NumProbesPerDimension*Nd;

            XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double');

            XiMin(1) = 0.5; XiMax(1) = 3; %dipole length, L, in Wavelengths
            XiMin(2) = 0  ; XiMax(2) = Pi/2; %polar angle, Theta, in Radians

            StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

            %NN = FREEFILE; OPEN 'INFILE.DAT' FOR OUTPUT AS NN; PRINT NN,'PBM1.NEC'; PRINT NN,'PBM1.OUT'; CLOSE NN %NEC Input/Output Files

        case 'PBM_2' %2-D

            AddNoiseToPBM2 = 'NO'; %'YES' %'NO' %'YES'

            Nd                    = 2;
            NumProbesPerDimension = 4; %20
            Np                    = NumProbesPerDimension*Nd;

            Nt      = 100;
            G        = 2;
            Alpha    = 2;
            Beta     = 2;
            DeltaT   = 1;
            Frep     = 0.5;

            PlaceInitialProbes  = 'UNIFORM ON-AXIS';
            InitialAcceleration = 'ZERO';
            RepositionFactor    = 'VARIABLE'; %'FIXED'

            Np = NumProbesPerDimension*Nd;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double');

            XiMin(1) = 5; XiMax(1) = 15; %dipole separation, D, in Wavelengths
            XiMin(2) = 0; XiMax(2) = Pi;   %polar angle, Theta, in Radians

            StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

            %NN = FREEFILE; OPEN 'INFILE.DAT' FOR OUTPUT AS NN; PRINT NN,'PBM2.NEC'; PRINT NN,'PBM2.OUT'; CLOSE NN

        case 'PBM_3' %2-D

            Nd                    = 2;
            NumProbesPerDimension = 4; %20
            Np                    = NumProbesPerDimension*Nd;

            Nt      = 100;
            G        = 2;
            Alpha    = 2;
            Beta     = 2;
            DeltaT   = 1;
            Frep     = 0.5;

            PlaceInitialProbes  = 'UNIFORM ON-AXIS';
            InitialAcceleration = 'ZERO';
            RepositionFactor    = 'VARIABLE'; %'FIXED'

            Np = NumProbesPerDimension*Nd;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double');

            XiMin(1) = 0; XiMax(1) = 4; %Phase Parameter, Beta (0-4)
            XiMin(2) = 0; XiMax(2) = Pi;  %polar angle, Theta, in Radians

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

            %NN = FREEFILE; OPEN 'INFILE.DAT' FOR OUTPUT AS NN; PRINT NN,'PBM3.NEC'; PRINT NN,'PBM3.OUT'; CLOSE NN

        case 'PBM_4' %2-D

            Nd                    = 2;
            NumProbesPerDimension = 4; %6 %2 %4 %20
            Np                    = NumProbesPerDimension*Nd;

            Nt      = 100;
            G        = 2;
            Alpha    = 2;
            Beta     = 2;
            DeltaT   = 1;
            Frep     = 0.5;

            PlaceInitialProbes  = 'UNIFORM ON-AXIS';
            InitialAcceleration = 'ZERO';
            RepositionFactor    = 'VARIABLE'; %'FIXED'

            Np = NumProbesPerDimension*Nd

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double')

            XiMin(1) = 0.5  ; XiMax(1) = 1.5  %ARM LENGTH (NOT Total Length), wavelengths (0.5-1.5)
            XiMin(2) = Pi/18; XiMax(2) = Pi/2 %Inner angle, Alpha, in Radians (Pi/18-Pi/2)

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

            NN = FREEFILE; OPEN 'INFILE.DAT' FOR OUTPUT AS NN; PRINT NN,'PBM4.NEC'; PRINT NN,'PBM4.OUT'; CLOSE NN

        case 'PBM_5'

            NumCollinearElements  = 6; %30 %EVEN or ODD: 6,7,10,13,16,24 used by PBM

            Nd                    = NumCollinearElements - 1;
            NumProbesPerDimension = 4; %20
            Np                    = NumProbesPerDimension*Nd;

            Nt      = 100;
            G        = 2;
            Alpha    = 2;
            Beta     = 2;
            DeltaT   = 1;
            Frep     = 0.5;

            PlaceInitialProbes  = 'UNIFORM ON-AXIS';
            InitialAcceleration = 'ZERO';
            RepositionFactor    = 'VARIABLE'; %'FIXED'

            Nd = NumCollinearElements - 1;

            Np = NumProbesPerDimension*Nd;

             XiMin = zeros(Nd,'double'); XiMax = zeros(Nd,'double'); for i=1:Nd XiMin(i) = 0.5; XiMax(i) = 1.5; end

             StartingXiMin = zeros(Nd,'double'); StartingXiMax = zeros(Nd,'double'); for i=1:Nd StartingXiMin(i) = XiMin(i); StartingXiMax(i) = XiMax(i); end

            %NN = FREEFILE; OPEN 'INFILE.DAT' FOR OUTPUT AS NN; PRINT NN,'PBM5.NEC'; PRINT NN,'PBM5.OUT'; CLOSE NN

%   ======================================================================================
%   NOTE - DON%T FORGET TO ADD NEW TEST FUNCTIONS TO FUNCTION ObjectiveFunction() ABOVE !!
%   ======================================================================================

    end

    if Nd > 100
        Nt = min(Nt,200); %to avoid array dimensioning problems
    end

    DiagLength = 0; 
        for i=1:Nd DiagLength = DiagLength + (XiMax(i)-XiMin(i))^2; 
    end; 
    DiagLength = sqrt(DiagLength); %compute length of decision space principal diagonal

end

