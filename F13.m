% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COpiED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function Z = F13(R, Nd, p, j)

    X1 = R(p,1,j);
    Xn = R(p,Nd,j);

    Sum1 = 0;

    for i=1:Nd-1

        Xi  = R(p,i,j);
        XiPlus1 = R(p,i+1,j);

        Sum1 = Sum1 + (Xi-1)^2*(1+(sin(3*pi*XiPlus1))^2);

    end

    Sum1 = Sum1 + (sin(pi*3*X1))^2 +(Xn-1)^2*(1+(sin(2*pi*Xn))^2);

    Sum2 = 0;

    for i=1:Nd

        Xi = R(p,i,j);

        Sum2 = Sum2 + u(Xi,5,100,4);

    end

    Z = Sum1/10 + Sum2;

    Z = -Z;
end