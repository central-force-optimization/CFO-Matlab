% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function [ Z ] = F21( R, Nd, p, j )
% UNTITLED Summary of this function goes here
%    Detailed explanation goes here

    m = 5; 
    Aji = zeros(m,4); 
    Cj = zeros(m);

    Aji(1,1)  = 4; Aji(1,2)  =   4; Aji(1,3)  = 4; Aji(1,4)  =   4; Cj(1)  = 0.1;
    Aji(2,1)  = 1; Aji(2,2)  =   1; Aji(2,3)  = 1; Aji(2,4)  =   1; Cj(2)  = 0.2;
    Aji(3,1)  = 8; Aji(3,2)  =   8; Aji(3,3)  = 8; Aji(3,4)  =   8; Cj(3)  = 0.2;
    Aji(4,1)  = 6; Aji(4,2)  =   6; Aji(4,3)  = 6; Aji(4,4)  =   6; Cj(4)  = 0.4;
    Aji(5,1)  = 3; Aji(5,2)  =   7; Aji(5,3)  = 3; Aji(5,4)  =   7; Cj(5)  = 0.4;

    Z = 0;

    for jj = 1 : m  %NOTE:  Index jj is used : avoid same variable name as j

        Sum = 0;

        for i = 1 : 4 %Shekel's family is 4-D only

            Xi = R(p,i,j);

            Sum = Sum + (Xi-Aji(jj,i))^2;

        end

        Z = Z + 1/(Sum + Cj(jj));

    end

end

