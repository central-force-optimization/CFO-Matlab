% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to; CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to; Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function Z = F15(R, Nd, p, j)

    Aj(1)  = 0.1957; Bj(1)  = 1/0.25;
    Aj(2)  = 0.1947; Bj(2)  = 1/0.50;
    Aj(3)  = 0.1735; Bj(3)  = 1/1.00;
    Aj(4)  = 0.1600; Bj(4)  = 1/2.00;
    Aj(5)  = 0.0844; Bj(5)  = 1/4.00;
    Aj(6)  = 0.0627; Bj(6)  = 1/6.00;
    Aj(7)  = 0.0456; Bj(7)  = 1/8.00;
    Aj(8)  = 0.0342; Bj(8)  = 1/10.0;
    Aj(9)  = 0.0323; Bj(9)  = 1/12.0;
    Aj(10) = 0.0235; Bj(10) = 1/14.0;
    Aj(11) = 0.0246; Bj(11) = 1/16.0;

    Z = 0;

    x1 = R(p,1,j);
    x2 = R(p,2,j);
    x3 = R(p,3,j);
    x4 = R(p,4,j);

    for jj=1:11

       Num   = x1*(Bj(jj)^2+Bj(jj)*x2);

       Denom = Bj(jj)^2+Bj(jj)*x3+x4;

       Z = Z + (Aj(jj)-Num/Denom)^2;

    end

    Z = -Z;
end