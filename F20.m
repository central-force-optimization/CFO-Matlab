% This program implements a version of "Central Force Optimization" as developed 
% by Richard A. Formato. The code for this Matlab version was developed by
% Robert C. Green II. It is distributed free of charge to increase awareness
% and encourage experimentation with the algorithm.
% 
% CFO is a multi-dimensional search and optimization algorithm that locates the
% global maxima of a function. Unlike most other algorithms, CFO is completely
% deterministic so that every run with the same setup produces the same results.
% 
% Please email questions, comments, and significant results to: CFO_questions@yahoo.com.  
% Any questions regarding the C++ version can be emailed to: Robert.C.Green@gmail.com
% Thanks!
% 
% (c) 2006-2009 Richard A. Formato
% 
% ALL RIGHTS RESERVED WORLDWIDE
% 
% THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND DISTRIBUTED WITHOUT LIMITATION AS 
% LONG AS THIS COPYRIGHT NOTICE AND THE GNUPLOT AND REFERENCE INFORMATION BELOW ARE 
% INCLUDED WITHOUT MODIFICATION, AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
% INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR OTHER PRODUCTS

function [ Z ] = F20( R, Nd, p, j )
% F20 Summary of this function goes here
%    Detailed explanation goes here
    
    Aji = zeros(4,6,'double');
    Cj = zeros(4, 'double');
    Pji = zeros(4,6, 'double');

    Aji(1,1) = 10.0;Aji(1,2) = 3.00;Aji(1,3) = 17.0;Cj(1) = 1.0;
    Aji(2,1) = 0.05;Aji(2,2) = 10.0;Aji(2,3) = 17.0;Cj(2) = 1.2;
    Aji(3,1) = 3.00;Aji(3,2) = 3.50;Aji(3,3) = 1.70;Cj(3) = 3.0;
    Aji(4,1) = 17.0;Aji(4,2) = 8.00;Aji(4,3) = 0.05;Cj(4) = 3.2;

    Aji(1,4) = 3.5;Aji(1,5) = 1.7;Aji(1,6) =  8;
    Aji(2,4) = 0.1;Aji(2,5) =   8;Aji(2,6) = 14;
    Aji(3,4) =  10;Aji(3,5) =  17;Aji(3,6) =  8;
    Aji(4,4) =  10;Aji(4,5) = 0.1;Aji(4,6) = 14;

    Pji(1,1) = 0.13120;Pji(1,2) = 0.1696;Pji(1,3) = 0.5569;
    Pji(2,1) = 0.23290;Pji(2,2) = 0.4135;Pji(2,3) = 0.8307;
    Pji(3,1) = 0.23480;Pji(3,2) = 0.1415;Pji(3,3) = 0.3522;
    Pji(4,1) = 0.40470;Pji(4,2) = 0.8828;Pji(4,3) = 0.8732;

    Pji(1,4) = 0.01240;Pji(1,5) = 0.8283;Pji(1,6) = 0.5886;
    Pji(2,4) = 0.37360;Pji(2,5) = 0.1004;Pji(2,6) = 0.9991;
    Pji(3,4) = 0.28830;Pji(3,5) = 0.3047;Pji(3,6) = 0.6650;
    Pji(4,4) = 0.57430;Pji(4,5) = 0.1091;Pji(4,6) = 0.0381;

    Z = 0;

    for jj = 1:4

        Sum = 0;

        for i = 1:6

            Xi = R(p,i,j);

            Sum = Sum + Aji(jj,i)*(Xi-Pji(jj,i))^2;

        end

        Z = Z + Cj(jj)*exp(-Sum);

    end

end

