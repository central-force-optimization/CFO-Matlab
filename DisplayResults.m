function  DisplayResults( functionName, fitness, probe, timeStep,Gamma, ProbesPerAxis, Np,lastStep )
    
    str = sprintf('Function Name:          %s', functionName);
    disp(str);
    
    str = sprintf('Best Overall Fitness:   %f', fitness);
    disp(str);
    
    str = sprintf('Best Overall Probe #:   %i', probe);
    disp(str);
    
    str = sprintf('Best Overall Time Step: %i', timeStep);
    disp(str);
    
    str = sprintf('Best Overall Gamma: %f', Gamma);
    disp(str);
    
    str = sprintf('Best Overall Probes Per Axis: %i', ProbesPerAxis);
    disp(str);
    
    str = sprintf('Best Number of Probes: %i', Np);
    disp(str)
    
    str = sprintf('Best Overall Last Step: %i', lastStep);
    disp(str)
    
    disp(' ');


end

